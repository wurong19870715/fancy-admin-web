export default[
    {
        path: '/',
        component: resolve => require(['@/components/common/Template.vue'], resolve),
        meta: { title: '首页' },
        children:[
            {
                path: '/sys/org/',
                redirect: '/sys/org/sys_org_element'
            },
            {
                name: 'sysOrgPostList',
                path: '/sys/org/sys_org_post',
                component: resolve => require(['@/page/sys/org/sys_org_post/SysOrgPost_list.vue'], resolve),
                meta: { title: '岗位管理',keepAlive: true }
            },
            {
                name: 'sysOrgElementList',
                path: '/sys/org/sys_org_element',
                component: resolve => require(['@/page/sys/org/sys_org_element/SysOrgElement_list.vue'], resolve),
                meta: { title: '机构管理',keepAlive: true }
            },
            {
                name: 'sysOrgElementAdd',
                path: '/sys/org/sys_org_element/edit',
                component: resolve => require(['@/page/sys/org/sys_org_element/SysOrgElement_edit.vue'], resolve),
                meta: { title: '机构维护',keepAlive: false }
            },
            {
                name: 'sysOrgDeptList',
                path: '/sys/org/sys_org_dept',
                component: resolve => require(['@/page/sys/org/sys_org_dept/SysOrgDept_list.vue'], resolve),
                meta: { title: '部门管理',keepAlive: true }
            },
            {
                name: 'sysOrgPersonList',
                path: '/sys/org/sys_org_person',
                component: resolve => require(['@/page/sys/org/sys_org_person/SysOrgPerson_list.vue'], resolve),
                meta: { title: '人员管理',keepAlive: true }
            },
            {
                name: 'sysOrgPersonInfo',
                path: '/sys/org/personInfo',
                component: resolve => require(['@/page/sys/org/sys_org_person/SysOrgPerson_Info.vue'], resolve),
                meta: { title: '个人中心',keepAlive: false }
            },{
                name: 'sysOrgPostAdd',
                path: '/sys/org/sys_org_post/edit',
                component: resolve => require(['@/page/sys/org/sys_org_post/SysOrgPost_edit.vue'], resolve),
                meta: { title: '岗位维护',keepAlive: false }
            },
            {
                name: 'sysOrgPostView',
                path: '/sys/org/sys_org_post/view',
                component: resolve => require(['@/page/sys/org/sys_org_post/SysOrgPost_view.vue'], resolve),
                meta: { title: '岗位查看',keepAlive: false }
            },
            {
                name: 'sysOrgElementView',
                path: '/sys/org/sys_org_element/view',
                component: resolve => require(['@/page/sys/org/sys_org_element/SysOrgElement_view.vue'], resolve),
                meta: { title: '机构查看',keepAlive: false }
            },
            {
                name: 'sysOrgDeptAdd',
                path: '/sys/org/sys_org_dept/edit',
                component: resolve => require(['@/page/sys/org/sys_org_dept/SysOrgDept_edit.vue'], resolve),
                meta: { title: '部门维护',keepAlive: false }
            },
            {
                name: 'sysOrgDeptView',
                path: '/sys/org/sys_org_dept/view',
                component: resolve => require(['@/page/sys/org/sys_org_dept/SysOrgDept_view.vue'], resolve),
                meta: { title: '部门查看',keepAlive: false }
            },
            {
                name: 'sysOrgPersonAdd',
                path: '/sys/org/sys_org_person/edit',
                component: resolve => require(['@/page/sys/org/sys_org_person/SysOrgPerson_edit.vue'], resolve),
                meta: { title: '人员维护',keepAlive: false }
            },
            {
                name: 'sysOrgPersonView',
                path: '/sys/org/sys_org_person/view',
                component: resolve => require(['@/page/sys/org/sys_org_person/SysOrgPerson_view.vue'], resolve),
                meta: { title: '人员查看',keepAlive: false }
            }
        ]
    }
]
