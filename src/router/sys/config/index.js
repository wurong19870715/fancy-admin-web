export default[
    {
        path: '/',
        component: resolve => require(['@/components/common/Template.vue'], resolve),
        meta: { title: '首页' },
        children:[
            {
                path: '/sys/config',
                redirect:'/sys/config/sys_config_main'
            },
            {
                name: 'sysConfigMainList',
                path: '/sys/config/sys_config_main',
                component: resolve => require(['@/page/sys/config/sys_config_main/SysConfigMain_list.vue'], resolve),
                meta: { title: '参数管理',keepAlive: false }
            },
            {
                name: 'sysConfigMainAdd',
                path: '/sys/config/sys_config_main/edit',
                component: resolve => require(['@/page/sys/config/sys_config_main/SysConfigMain_edit.vue'], resolve),
                meta: { title: '参数维护',keepAlive: false }
            }
            
        ]
    }
]
