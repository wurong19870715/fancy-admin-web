export default[
    {
        path: '/',
        component: resolve => require(['@/components/common/Template.vue'], resolve),
        meta: { title: '首页' },
        children:[
            {
                path: '/sys/job',
                redirect:'/sys/job/sys_quartz_schedule_job'
            },
            {
                name: 'sysQuartzScheduleJobList',
                path: '/sys/job/sys_quartz_schedule_job',
                component: resolve => require(['@/page/sys/job/sys_quartz_schedule_job/SysQuartzScheduleJob_list.vue'], resolve),
                meta: { title: '定时任务',keepAlive: true }
            },
            {
                name: 'sysQuartzScheduleJobAdd',
                path: '/sys/job/sys_quartz_schedule_job/edit',
                component: resolve => require(['@/page/sys/job/sys_quartz_schedule_job/SysQuartzScheduleJob_edit.vue'], resolve),
                meta: { title: '维护任务',keepAlive: false }
            },
            {
                name: 'sysQuartzScheduleJobView',
                path: '/sys/job/sys_quartz_schedule_job/view',
                component: resolve => require(['@/page/sys/job/sys_quartz_schedule_job/SysQuartzScheduleJob_view.vue'], resolve),
                meta: { title: '查看任务',keepAlive: false }
            }
            
        ]
    }
]
