export default[
    {
        path: '/',
        component: resolve => require(['@/components/common/Template.vue'], resolve),
        meta: { title: '首页' },
        children:[
            {
                name: 'sysDataSourceMainList',
                path: '/sys/datasource',
                component: resolve => require(['@/page/sys/datasource/sys_data_source_main/SysDataSourceMain_list.vue'], resolve),
                meta: { title: '数据源列表',keepAlive: true }
            },
            {
                name: 'sysDataSourceMainEdit',
                path: '/sys/datasource/sys_data_source_main/edit',
                component: resolve => require(['@/page/sys/datasource/sys_data_source_main/SysDataSourceMain_edit.vue'], resolve),
                meta: { title: '数据源编辑',keepAlive: false }
            },
            {
                name: 'sysDataSourceMainView',
                path: '/sys/datasource/sys_data_source_main/view',
                component: resolve => require(['@/page/sys/datasource/sys_data_source_main/SysDataSourceMain_view.vue'], resolve),
                meta: { title: '数据源查看',keepAlive: false }
            }
        ]
    }
]
