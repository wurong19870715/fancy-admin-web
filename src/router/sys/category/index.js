export default[
    {
        path: '/form',
        component: resolve => require(['@/components/common/Template.vue'], resolve),
        children:[
            {
                name: 'sysCategoryMainAdd',
                path: '/sys/category/sys_category_main/edit',
                component: resolve => require(['@/page/sys/category/sys_category_main/SysCategoryMain_edit.vue'], resolve),
                meta: { title: '分类维护',keepAlive: false }
            },
            {
                name: 'sysCategoryMainView',
                path: '/sys/category/sys_category_main/view',
                component: resolve => require(['@/page/sys/category/sys_category_main/SysCategoryMain_view.vue'], resolve),
                meta: { title: '分类查看',keepAlive: false }
            }
        ]
    },
]
