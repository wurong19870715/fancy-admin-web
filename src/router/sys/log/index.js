export default[
    {
        path: '/',
        component: resolve => require(['@/components/common/Template.vue'], resolve),
        meta: { title: '首页' },
        children:[
            {
                path: '/sys/log',
                redirect:'/sys/log/sys_log_login_main'
            },
            {
                name: 'sysLogLoginMainList',
                path: '/sys/log/sys_log_login_main',
                component: resolve => require(['@/page/sys/log/sys_log_login_main/SysLogLoginMain_list.vue'], resolve),
                meta: { title: '登录日志',keepAlive: true }
            },
            {
                name: 'sysLogOperateMainList',
                path: '/sys/log/sys_log_operate_main',
                component: resolve => require(['@/page/sys/log/sys_log_operate_main/SysLogOperateMain_list.vue'], resolve),
                meta: { title: '操作日志',keepAlive: true }
            }
            
        ]
    }
]
