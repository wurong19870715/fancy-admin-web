export default[
    {
        path: '/',
        component: resolve => require(['@/components/common/Template.vue'], resolve),
        meta: { title: '首页' },
        children:[
            {
                path:'/sys/rest',
                redirect:'/sys/rest/sys_rest_main'
            },
            {
                name: 'sysRestLogList',
                path: '/sys/rest/sys_rest_log',
                component: resolve => require(['@/page/sys/rest/sys_rest_log/SysRestLog_list.vue'], resolve),
                meta: { title: '接口日志',keepAlive: true }
            },
            {
                name: 'sysRestLogView',
                path: '/sys/rest/sys_rest_log/view',
                component: resolve => require(['@/page/sys/rest/sys_rest_log/SysRestLog_view.vue'], resolve),
                meta: { title: '接口查看',keepAlive: false }
            },
            {
                name: 'sysRestPolicyList',
                path: '/sys/rest/sys_rest_policy',
                component: resolve => require(['@/page/sys/rest/sys_rest_policy/SysRestPolicy_list.vue'], resolve),
                meta: { title: '策略列表',keepAlive: true }
            },
            {
                name: 'sysRestPolicyEdit',
                path: '/sys/rest/sys_rest_policy/edit',
                component: resolve => require(['@/page/sys/rest/sys_rest_policy/SysRestPolicy_edit.vue'], resolve),
                meta: { title: '策略维护',keepAlive: false }
            },
            {
                name: 'sysRestPolicyView',
                path: '/sys/rest/sys_rest_policy/view',
                component: resolve => require(['@/page/sys/rest/sys_rest_policy/SysRestPolicy_view.vue'], resolve),
                meta: { title: '策略查看',keepAlive: false }
            },
            {
                name: 'sysRestMainList',
                path: '/sys/rest/sys_rest_main',
                component: resolve => require(['@/page/sys/rest/sys_rest_main/SysRestMain_list.vue'], resolve),
                meta: { title: '接口列表',keepAlive: true }
            },
            {
                name: 'sysRestMainView',
                path: '/sys/rest/sys_rest_main/view',
                component: resolve => require(['@/page/sys/rest/sys_rest_main/SysRestMain_view.vue'], resolve),
                meta: { title: '接口查看',keepAlive: false }
            },
            {
                name: 'sysRestMainEdit',
                path: '/sys/rest/sys_rest_main/edit',
                component: resolve => require(['@/page/sys/rest/sys_rest_main/SysRestMain_edit.vue'], resolve),
                meta: { title: '接口编辑',keepAlive: false }
            }
            
        ]
    }
]
