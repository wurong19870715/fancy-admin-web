export default[
    {
        path: '/',
        component: resolve => require(['@/components/common/Template.vue'], resolve),
        meta: { title: '首页' },
        children:[
            {
                path: '/sys/enums',
                redirect:'/sys/enums/sys_enums_main'
            },
            {
                name: 'sysEnumsMainList',
                path: '/sys/enums/sys_enums_main',
                component: resolve => require(['@/page/sys/enums/sys_enums_main/SysEnumsMain_list.vue'], resolve),
                meta: { title: '枚举列表',keepAlive: true }
            }
            ,
            {
                name: 'sysEnumsDataList',
                path: '/sys/enums/sys_enums_data',
                component: resolve => require(['@/page/sys/enums/sys_enums_data/SysEnumsData_list.vue'], resolve),
                meta: { title: '枚举值列表',keepAlive: true }
            }
            ,
            {
                name: 'sysEnumsMainAdd',
                path: '/sys/enums/sys_enums_main/edit',
                component: resolve => require(['@/page/sys/enums/sys_enums_main/SysEnumsMain_edit.vue'], resolve),
                meta: { title: '枚举维护',keepAlive: false }
            },
            {
                name: 'sysEnumsMainView',
                path: '/sys/enums/sys_enums_main/view',
                component: resolve => require(['@/page/sys/enums/sys_enums_main/SysEnumsMain_view.vue'], resolve),
                meta: { title: '枚举查看',keepAlive: false }
            },
            {
                name: 'sysEnumsDataAdd',
                path: '/sys/enums/sys_enums_data/edit',
                component: resolve => require(['@/page/sys/enums/sys_enums_data/SysEnumsData_edit.vue'], resolve),
                meta: { title: '枚举值维护',keepAlive: false }
            },
            {
                name: 'sysEnumsDataView',
                path: '/sys/enums/sys_enums_data/view',
                component: resolve => require(['@/page/sys/enums/sys_enums_data/SysEnumsData_view.vue'], resolve),
                meta: { title: '枚举值查看',keepAlive: false }
            }
            
        ]
    }
]
