export default[
    {
        path: '/',
        component: resolve => require(['@/components/common/Template.vue'], resolve),
        meta: { title: '首页' },
        children:[
            {
                path: '/sys/permission',
                redirect: '/sys/permission/sys_permission_role'
            }
            ,
            {
                name: 'sysPermissionRoleList',
                path: '/sys/permission/sys_permission_role',
                component: resolve => require(['@/page/sys/permission/sys_permission_role/SysPermissionRole_list.vue'], resolve),
                meta: { title: '角色列表',keepAlive: true }
            }
            ,
            {
                name: 'sysPermissionRoleCategoryList',
                path: '/sys/permission/sys_permission_role_category',
                component: resolve => require(['@/page/sys/permission/sys_permission_role_category/SysPermissionRoleCategory_list.vue'], resolve),
                meta: { title: '角色分类列表',keepAlive: true }
            },
            {
                name: 'sysPermissionRoleAdd',
                path: '/sys/permission/sys_permission_role/edit',
                component: resolve => require(['@/page/sys/permission/sys_permission_role/SysPermissionRole_edit.vue'], resolve),
                meta: { title: '角色维护',keepAlive: false }
            },
            {
                name: 'sysPermissionRoleView',
                path: '/sys/permission/sys_permission_role/view',
                component: resolve => require(['@/page/sys/permission/sys_permission_role/SysPermissionRole_view.vue'], resolve),
                meta: { title: '角色查看',keepAlive: false }
            },
            {
                name: 'sysPermissionRoleCategoryAdd',
                path: '/sys/permission/sys_permission_role_category/edit',
                component: resolve => require(['@/page/sys/permission/sys_permission_role_category/SysPermissionRoleCategory_edit.vue'], resolve),
                meta: { title: '角色分类维护',keepAlive: false }
            }
            
        ]
    }
]
