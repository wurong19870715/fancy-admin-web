export default[
  {
    path: '/',
    component: resolve => require(['@/components/common/Template.vue'], resolve),
    meta: { title: '首页' },
    children:[
        {
            name: 'designer',
            path: '/designerExample',
            component: resolve => require(['@/page/admin/form/designerExample.vue'], resolve),
            meta: { title: '表单集成demo',keepAlive: true }
        }
    ]
  }
]
