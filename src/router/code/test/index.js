export default[
    {
        path: '/',
        component: resolve => require(['@/components/common/Template.vue'], resolve),
        meta: { title: '首页' },
        children:[
                        {
                name: 'demoTestList',
                path: '/code/test',
                component: resolve => require(['@/page/code/test/demo_test/DemoTest_list.vue'], resolve),
                meta: { title: 'demo_test',keepAlive: true }
            },
            {
                name: 'demoTestEdit',
                path: '/code/test/demo_test/edit',
                component: resolve => require(['@/page/code/test/demo_test/DemoTest_edit.vue'], resolve),
                meta: { title: '新增',keepAlive: false }
            },
            {
                name: 'demoTestView',
                path: '/code/test/demo_test/view',
                component: resolve => require(['@/page/code/test/demo_test/DemoTest_view.vue'], resolve),
                meta: { title: '查看',keepAlive: false }
            }
        ]
    }
]
