export default[
  {
    path: '/',
    component: resolve => require(['@/components/common/Template.vue'], resolve),
    meta: { title: '首页' },
    children:[
        {
            name: 'codeGenerateList',
            path: '/code/generate',
            component: resolve => require(['@/page/code/generate/List.vue'], resolve),
            meta: { title: '模块列表',keepAlive: true }
        },
        {
            name: 'codeGenerateTable',
            path: '/code/generate/tables',
            component: resolve => require(['@/page/code/generate/TableList.vue'], resolve),
            meta: { title: '表格列表',keepAlive: true }
      }
    ]
  }
]
