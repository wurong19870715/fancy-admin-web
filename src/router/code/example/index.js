export default[
  {
    path: '/',
    component: resolve => require(['@/components/common/Template.vue'], resolve),
    meta: { title: '首页' },
    children:[
        {
            name: 'codeExampleList',
            path: '/code/example',
            component: resolve => require(['@/page/code/example/List.vue'], resolve),
            meta: { title: '代码Demo',keepAlive: true }
        },{
            name: 'codeExamplePersonAdd',
            path: '/code/example/edit',
            component: resolve => require(['@/page/code/example/Edit.vue'], resolve),
            meta: { title: '新增Demo',keepAlive: false }
        },
        {
            name: 'codeExamplePersonView',
            path: '/code/example/view',
            component: resolve => require(['@/page/code/example/View.vue'], resolve),
            meta: { title: '查看Demo',keepAlive: false }
        }
    ]
  }
]
