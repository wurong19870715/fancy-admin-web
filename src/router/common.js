export default[
  {
    path: '/',
    redirect:'/home'
  },
  {
    path: '/',
    component: resolve => require(['../components/common/Template.vue'], resolve),
    children:[
        {
            name: 'home',
            path: '/Home',
            component: resolve  => require(['../page/Home.vue'], resolve),
            meta: { title: '首页',keepAlive: true  }
        }
    ]
  },
  {
    path: '/login',
    component: resolve => require(['../page/Login.vue'], resolve)
  },
  {
    path: '/',
    component: resolve => require(['../components/common/Template.vue'], resolve),
    children:[
        {
            name: 'sysPortal',
            path: '/sys',
            component: resolve  => require(['../page/SysPortal.vue'], resolve),
            meta: { title: '后台管理',keepAlive: true  }
        },
        {
            name: 'systemConfig',
            path: '/systemConfig',
            component: resolve => require(['../page/sys/SystemConfig.vue'], resolve),
            meta: { title: '系统配置',keepAlive: false  }
        },
        {
            path: '/403',
            component: resolve => require(['../page/403.vue'], resolve),
            meta: { title: '403',keepAlive: false  }
        },
        {
            path: '/404',
            component: resolve => require(['../page/404.vue'], resolve),
            meta: { title: '404',keepAlive: false  }
        }
    ]
  },
    {
        path: '*',
        redirect: '/404'
    }
]
