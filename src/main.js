import Vue from 'vue'
import App from './App.vue'

import axios from 'axios';
import ElementUI from 'element-ui';
import VueI18n from 'vue-i18n';
import store from  './store'
import 'element-ui/lib/theme-chalk/index.css'; // 默认主题
import "babel-polyfill";
//自定义样式
import './assets/cdn/font_830376_qzecyukz0s.css';
import './assets/css/icon.css';
import './assets/css/iconfont/iconfont.js';
import './assets/iconfont/iconfont.js';
//自定义指令
import './components/common/directives';

//自定义函数
import constant from './utils/constant';
import commonutils from './utils/commonUtils';
import {post,fetch,patch,put,del,download,fetchImg} from './utils/http';
//自定义组件
import fancyAddress from './components/form/index.js';
import designer from './components/common/formDesigner/index';


//路由
import router from './router'

Vue.config.productionTip = false
Vue.use(VueI18n);
Vue.use(fancyAddress);
Vue.use(designer);
Vue.use(ElementUI, {
    size: 'small'
});

Vue.prototype.$axios = axios;
Vue.prototype.$constant = constant;
Vue.prototype.$common = commonutils;
Vue.prototype.$post = post;
Vue.prototype.$fetch = fetch;
Vue.prototype.$patch = patch;
Vue.prototype.$put = put;
Vue.prototype.$delete = del;
Vue.prototype.$download = download;
Vue.prototype.$fetchImg = fetchImg;



// const i18n = new VueI18n({
//     locale: 'zh',
//     messages
// })




new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')