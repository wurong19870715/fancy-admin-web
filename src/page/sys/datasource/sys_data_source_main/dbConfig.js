const dbSourceConfig = [
	{fdType:"Oracle",fdDriver:"oracle.jdbc.driver.OracleDriver",fdUrl:"jdbc:oracle:thin:@localhost:1521:orcl"},
	{fdType:"SQLServer",fdDriver:"net.sourceforge.jtds.jdbc.Driver",fdUrl:"jdbc:jtds:sqlserver://localhost:1433/test"},
	{fdType:"MySQL",fdDriver:"com.mysql.cj.jdbc.Driver",fdUrl:"jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=UTF-8"},
	{fdType:"MySQL8",fdDriver:"com.mysql.cj.jdbc.Driver",fdUrl:"jdbc:mysql://localhost:3306/ekp?userSSL=true&autoReconnect=true&useUnicode=true&characterEncoding=UTF8&serverTimezone=Asia/Shanghai"},	
    {fdType:"其他",fdDriver:"",fdUrl:""}
]
export {dbSourceConfig}