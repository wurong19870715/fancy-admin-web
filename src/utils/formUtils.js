import dateUtils from '@/utils/dateUtils';
let formUtils = {
  //年月日
  convertToModel(obj) {
    if(obj === null) return null
    if(typeof obj !== 'object') return obj;
    if(obj.constructor===Date) return new Date(obj); 
    let newObj = {};  
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {   
            let val = obj[key];
            if(val.constructor === Object)continue;
            if(val.constructor === Array){
              if(val.length>0){
                if(val[0].constructor === Object){
                  newObj[key] = [];
                  for (let i in val){
                    let model = formUtils.convertToModel(val[i]);
                    newObj[key].push(model);
                  }
                }else{
                  let v = '';
                  for (let i in val){
                    if(val[i].constructor === String){
                      v= v+';'+val[i];
                    }
                  }
                  if(v.length>1){
                    v = v.substring(1);
                  }
                  newObj[key] = v;
                }
              
              }
            }else if(val.constructor === Date){
              newObj[key] = dateUtils.formatDateTime(val);
            }else{
              newObj[key] = val;
            }
        }
    }  
    return newObj;
  },
  //转换成表单
  convertToForm(sourceObj,targetObj) {
    if(sourceObj === null) return
    if(typeof sourceObj !== 'object') return ;
    if(sourceObj.constructor===Date) return ; 
    
    for (let key in sourceObj) {
          if (sourceObj.hasOwnProperty(key)) {//如果表单中没有这个key,就跳过   
            let sourceVal = sourceObj[key];
            if(typeof sourceVal === 'undefined'|| sourceVal == null){
              continue;
            }
            if(typeof targetObj[key] == 'undefined'|| targetObj[key] == null){
              continue;
            }
            if (sourceVal.constructor === Date){
              targetObj[key] = dateUtils.formatDateTime(sourceVal);
            }else{
              if(targetObj[key].constructor === Array) {
                if(sourceVal.length>0){
                  if(sourceVal[0].constructor === Object){
                    let fieldObj = formUtils.cloneObj(targetObj[key][0]);
                    targetObj[key] = [];
                    for (let i in sourceVal){
                      let cloneObj = formUtils.cloneObj(fieldObj);
                      formUtils.convertToForm(sourceVal[i],cloneObj);
                      targetObj[key].push(cloneObj);
                    }
                  }else{
                    let splitStr = ';';
                    let str = sourceVal.split(splitStr);
                    str.forEach(v =>{
                      if(v !== ''){
                        targetObj[key].push(v);
                      }
                    });
                  }
                }
              }else if(sourceObj[key].constructor === Boolean||targetObj[key].constructor === Boolean){
                if(targetObj[key].constructor === Boolean){
                  targetObj[key] = sourceVal === 'true';
                }else{
                  targetObj[key] = sourceVal;
                }
              }else{
                targetObj[key] = String(sourceVal);
              }
            }
        }
    }  
  },
  //转换成表单
  isNotEmpty(obj){
      return !(typeof obj == "undefined" || obj == null || obj === "");
  },
  /**
   * 下划线转驼峰
   */
  toCamelCase(name) {
      return name.replace(/_(\w)/g, function(all, letter){
          return letter.toUpperCase();
      });
  },
    /**
   * 驼峰转下划线
   */
  toLineCase(name) {
    return name.replace(/([A-Z])/g,"_$1").toLowerCase();
  },
  cloneObj(obj) {
    let newObj = {};
    if (obj instanceof Array) {  
      newObj = [];  
    }
    for (let key in obj) {  
      let val = obj[key];  
      newObj[key] = typeof val === 'object' ? cloneObj(val): val;  
    }
    return newObj;
  }
 
}
export default formUtils;