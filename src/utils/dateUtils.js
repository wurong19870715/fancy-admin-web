import moment from 'moment';
import formUtils from './formUtils'

let dateUtil = {
  //年月日
  formatDate(date) {
    let d = '';
    if(formUtils.isNotEmpty(date)){
      d = moment(date).format("YYYY-MM-DD");
    }
    return d;
  },
  //默认格式化年月日时分
  format(date) {
    let d = '';
    if(formUtils.isNotEmpty(date)){
      d = moment(date).format("YYYY-MM-DD HH:mm");
    }
    return d;
  },

  //年月日时分秒
  formatDateTime(date) {
    let d = '';
    if(formUtils.isNotEmpty(date)){
      d = moment(date).format("YYYY-MM-DD HH:mm:ss");
    }
    return d;
  },
  
  //获取当前日期
  today(){
    return moment().format("YYYY-MM-DD");
  },
  //获取当前日期
  now(){
    return moment().format("YYYY-MM-DD HH:mm");
  },
}
export default dateUtil;