import CryptoJS from 'crypto-js';
import constant from '@/utils/constant';
const key = CryptoJS.enc.Utf8.parse(constant.secKey);  //十六位十六进制数作为密钥
const iv = CryptoJS.enc.Utf8.parse(constant.secIv);   //十六位十六进制数作为密钥偏移量

/**
 * 暂不启用
 */

export function base64_encrypt(text){
  let wordArray = CryptoJS.enc.Utf8.parse(text);
  text = CryptoJS.enc.Base64.stringify(wordArray);
  return text;
}
export function base64_decrypt(base64Text){
  let parsedWordArray = CryptoJS.enc.Base64.parse(base64Text);
  base64Text = parsedWordArray.toString(CryptoJS.enc.Utf8);
  return base64Text;
}

let secretUtil = {
  //加密
  encrypt(word){
    return "";
  },
  
  //解密
  Decrypt(word){
    return "";
  }
  
}

//export default secretUtil;