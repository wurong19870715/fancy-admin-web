/**
 * 启动后加载一些参数
 */
import store from '../store/'
import userUtils from './userUtil';
class Loader {
    constructor(){
        
    }
    /**
     * 加载菜单
     */
    fillMenus(){
        let menusConf = [];
        if(!store.getters.isInitMenus){
            const menus = require.context('../page', true, /.menu.json$/);
            menus.keys().forEach(obj=>{
                menusConf.push(menus(obj));
            });
            store.commit('setMenus',menusConf);
        }
    }

    loadMenusByType(type){
        let rtnMenu = {};
        const menus = store.getters.menus;
        menus.forEach(obj=>{
            if(obj.type===type){
                rtnMenu = obj.menu
            }
        })
        return rtnMenu;
    }
    loadTopMenusByType(){
        let rtnMenu = []
        const menus = store.getters.menus;
        const permissions = store.getters.getPermissions;
        menus.forEach(obj=>{
            const topMenu = obj.topMenu;
            if(!userUtils.isAdmin()&&topMenu.hasOwnProperty('auth')){ //如果有这个字段、则进行权限校验
                const auth = topMenu.auth;
                if(permissions.length>0&&permissions.includes(auth)){
                    rtnMenu.push(topMenu);
                }
            }else{
                rtnMenu.push(topMenu);
            }
            
        })
        return rtnMenu;
    }
    
    loadMenusByTopIndex(index){
        let rtnMenu = {}
        const menus = store.getters.menus;
        menus.forEach(obj=>{
            if(obj.topMenu.index===index){
                rtnMenu = obj.menu
            }
        })
        return rtnMenu;
    }
}

export default new Loader();
