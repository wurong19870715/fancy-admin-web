export default function (h,{column,$index}) {
  return h("span",[
    h("span",{
      "style":{
        "color":"#f56c6c"
      }
    },"*"),
    h("span",column.label),
  ])
}