import bus from '@/components/common/bus';
import router from "@/router";
let commonUtils = {
    openTag(params) {
        let routeUrl = router.resolve(params);
        bus.$emit("addTag",routeUrl);
    },
    openParamsTag(params) {
        let routeUrl = router.resolve(params);
        bus.$emit("addTag",routeUrl,params);

    },
    closeTag() {
        bus.$emit("close_current_tags");
    },
    isAdmin(){

    }
  }
  export default commonUtils;