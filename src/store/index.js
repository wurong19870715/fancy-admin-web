import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const store = new Vuex.Store({
  state:{
    type: 'fancyInfo',
    permissions:[],
    keepAlive:[],
    isInitMenus:false,
    menus:[]
  },
  getters:{
    getType:state=> state.type,
    getPermissions:state=> state.permissions,
    keepAlive: state => state.keepAlive,
    menus: state => state.menus,
    isInitMenus: state => state.isInitMenus
  },
  mutations:{
    setType:(state,type)=>state.type = type,
    setPermissions:(state,permissions)=>state.permissions = state.permissions.concat(permissions),
    setKeepAlive: (state, name)=>{
      const index = state.keepAlive.findIndex(item=>item ===name);
      if(index<0){
        state.keepAlive.push(name)
      }
    },
    removeKeepAlive:(state, name)=>{
      const index = state.keepAlive.findIndex(item=>item ===name);
      state.keepAlive.splice(index,1);
    },
    removeAllKeepAlive:(state)=>{
      state.keepAlive = [];
    },
    removeOtherKeepAlive:(state,name)=>{
      state.keepAlive = state.keepAlive.filter(item => {
        return item === name;
      });
    },
    setMenus:(state,menus)=>state.menus = menus,
    setIsInitMenus:(state,isInitMenus)=>state.isInitMenus = isInitMenus
  },
  actions:{
    changetype({commit,state},type) {
      commit('setType',type);
    }
  },
  plugins: [createPersistedState({
    storage: window.sessionStorage,
    reducer(val) {
      return {
        permissions: val.permissions
      }
    }
  })]
})
//console.log(this.$store.getters.getPermissions);
//this.$store.commit('setType','test11232111aaa');  同步，mutations
//this.$store.dispatch("changetype", 'test11232');  异步，actions
export default store