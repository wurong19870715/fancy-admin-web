import dateUtils from '@/utils/dateUtils';
import formUtils from '@/utils/formUtils';

export const view = {
    activated(){
        this.initForm();
    },
    mounted() {
        this.initForm();
    },
    methods: {
        handlerEdit() {
            const params = {
                path: this.editUrl,
                query: {id:this.form.fdId}
            }
            this.$router.push(params);
        },
        initForm() {
            let fdId = this.$route.query.id;
            if(formUtils.isNotEmpty(fdId)){
                this.$fetch(this.fetchUrl,{
                    id:fdId
                }).then((res) => {
                    formUtils.convertToForm(res.data,this.form);
                });
            }
        }
    },
    filters:{
        formatDate(time) {
            return dateUtils.formatDate(time);
        }
    }
}
export const edit = {
    mounted() {
        this.initForm();
    },
    methods: {
        handlerSubmitForm() {
            this.$refs['form'].validate((valid) => {
                if (valid) {
                    let forms = formUtils.convertToModel(this.form);
                    this.$put(this.updateUrl,forms).then((res) => {
                        if(res.status===1){
                            this.$message({
                                duration:1000,
                                message: res.message,
                                type: 'success',
                                onClose:()=>{
                                    this.$common.closeTag();
                                }
                            });
                        }else{
                            this.$message.error(res.message);
                        }
                    });

                }
            });
        },
        initForm() {
            let fdId = this.$route.query.id;
            if(formUtils.isNotEmpty(fdId)){
                this.$fetch(this.fetchUrl,{
                    id:fdId
                }).then((res) => {
                    console.log(res.data);
                    formUtils.convertToForm(res.data,this.form);
                    console.log(this.form);
                });
            }
        }
    }
}
export const list = {
    data () {
        return{
            tableData:{
                keyword: '',
                size:0,
                total:0,
                current:1,
                records: []
            },
            keyword: ''
        }
    },
    mounted() {
        this.getData();
    },
    activated() {
        this.getData();
    },
    methods: {
        add() {
            const params = {
                path: this.editUrl,
                query: {fdParentId:this.currentTreeValue}
            }
            this.$common.openTag(params);
        },
        getData() {
            const queryObj = {
                keyword: this.tableData.keyword,
                current: this.tableData.current,
                fdParentId: this.currentTreeValue
            };
            this.$post(this.listUrl,queryObj).then((res) => {
                this.tableData = res.data;
            });
        },
        query(){
            this.tableData.keyword = this.keyword;
            this.getData();
        },
        dateFormat(obj){
            return dateUtils.format(obj.fdCreateTime);
        },
        handleEdit(row){
            let id = row.fdId;
            this.$common.openTag({
                path: this.editUrl,
                query: {id:id}
            });
        },
        handleView(row){
            let id = row.fdId;
            this.$common.openTag({
                path: this.viewUrl,
                query: {id:id}
            });
        },
        handleDelete(index,row){
            this.$confirm('此操作将删除该条记录,是否继续?').then(() => {
                this.$delete(this.deleteUrl,{
                    id:row.fdId
                }).then(() => {
                    this.tableData.records.splice(index,1)
                    this.$message({type:'success',message:'删除成功!'})
                });
            })
        },
        handleCurrentChange(value){
            this.tableData.current = value;
            this.tableData.keyword = this.keyword;
            this.getData();
        }
    }
}

//编辑页面获取父级id
export const parentEdit = {
    activated(){
        this.initParent();
    },
    // mounted() {
    //     this.initParent();
    // },
    methods: {
        initParent(){
            console.log(this.$route.query);
            let fdParentId = this.$route.query.fdParentId;
            if(formUtils.isNotEmpty(fdParentId)){
              this.$fetch(this.fetchParentUrl,{
                    id:fdParentId
                }).then((res) => {
                    console.log(res.data.fdId);
                    this.form.fdParentId = res.data.fdId;
                    this.form.fdParentName = res.data.fdName;
                });
            }
        }
    }
}
export const orgTree = {
    data () {
        return{
            currentTreeValue:'',
            props: {
                label: 'name'
            },
            RootNode: {"id":"","name":"组织架构",orgType:""}
        }
    },
    methods: {
        loadNode(node, resolve) {
          if (node.level === 0) {
            this.getTreeData({},()=>{
              let tree = [this.RootNode];
              resolve(tree);
            });
          } else {
            this.getTreeData(node.data,(data)=>resolve(data));
          }
        },
        handleNodeClick(data) {
            this.currentTreeValue = data.id;
            this.getData();
        },
        getTreeData(data,callback){
          let params = {
            fdParentId:data.id,
            fdParentType:data.orgType,
            fdOrgType:this.orgType
          }
          this.$fetch(this.treeDataUrl,params).then((res) => {
            if(res.status===1){
              return callback(res.data);
            }
          });
        }
    }
}