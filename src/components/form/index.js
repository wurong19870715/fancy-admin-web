import Vue from 'vue';
import fancyAddress from './FancyAddress.vue';


const plugins = {
  install : function (e) {
    Vue.component('FancyAddress',fancyAddress);
  }
};

export default plugins;