# FancyAdminWeb #
## [前端源码](https://gitee.com/wurong19870715/fancy-admin-web "前端源码")

## [后端源码](https://gitee.com/wurong19870715/fancy-admin "后端源码")


## 安装步骤
```javascript
git clone https://gitee.com/wurong19870715/fancy-admin-web.git      // 把模板下载到本地
cd fancy-admin-web    // 进入模板目录
npm install         // 安装项目依赖，等待安装完成之后
```
## 本地开发
``` javascript
// 开启服务器，浏览器访问 http://localhost:8098
npm run dev
```
## 账号/密码 
`admin/1`
## 构建生产
``` javascript
// 执行构建命令，生成的dist文件夹放在服务器下即可访问
npm run build
```

## 前端模板
初始模板: [vue-manage-system](https://github.com/lin-xin/vue-manage-system "vue-manage-system")


## 项目截图
### 登录界面
![登录界面](https://gitee.com/wurong19870715/fancy-admin-web/raw/master/public/img/login.png "登录界面")
### 首页
![首页](https://gitee.com/wurong19870715/fancy-admin-web/raw/master/public/img/dashboard.png "首页")
### 组织架构
![组织架构](https://gitee.com/wurong19870715/fancy-admin-web/raw/master/public/img/org.png "组织架构")
### 权限分配
![权限分配](https://gitee.com/wurong19870715/fancy-admin-web/raw/master/public/img/role.png "权限分配")
### 代码生成器
![代码生成器](https://gitee.com/wurong19870715/fancy-admin-web/raw/master/public/img/codeGenerate1.png "代码生成器1")
![代码生成器](https://gitee.com/wurong19870715/fancy-admin-web/raw/master/public/img/codeGenerate2.png "代码生成器2")
![代码生成器](https://gitee.com/wurong19870715/fancy-admin-web/raw/master/public/img/codeGenerate3.png "代码生成器3")

## 特别感谢
- 感谢 [lin-xin](https://github.com/lin-xin) 大佬提供的前端模板

## 交流
QQ群:816204482
## LICENSE
**MIT**