module.exports = {
    publicPath: './',
    productionSourceMap: false,
    devServer: {
        port: 8098,
        proxy: {
            '/api':{
                target: "http://localhost:8099",
                changeOrigin:true,
                pathRewrite:{
                    '^/api':''
                }
            }
        }
    },
    configureWebpack: {
        devtool: 'source-map'
    }
}